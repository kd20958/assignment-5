import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Gui extends JFrame implements ActionListener{
	private static JTextField txtScore[];
	private static JTextField txtWeight[];
	private JLabel lblScore[];
	private JButton btnCalculate;
	private JPanel pnlButton;
	private JPanel pnlScore;
	
	private final int SIZE = 4;
	
	public Gui() {
		txtScore = new JTextField[4];
		txtWeight = new JTextField[4];
		lblScore = new JLabel[4];
		
		for(int i=0; i<SIZE; i++) {
			txtScore[i] = new JTextField("Enter Score" + (i+1));
			txtWeight[i] = new JTextField("Enter Weight" + (i+1));
			lblScore[i] = new JLabel();
		}
		btnCalculate = new JButton("Click to Calculate");
		lblScore[0].setText("Your score is : ");
		lblScore[1].setText("Score");
		lblScore[2].setText("Weight");
		
		pnlButton = new JPanel();
		pnlScore = new JPanel();
		
		pnlButton.setBackground(Color.WHITE);
		pnlScore.setBackground(Color.CYAN);
		
		pnlScore.setLayout(new GridLayout(5,2));
		
		pnlScore.add(lblScore[1]);
		pnlScore.add(lblScore[2]);
		
		for(int i=0; i<SIZE; i++) {
			pnlScore.add(txtScore[i]);
			pnlScore.add(txtWeight[i]);
		}
		
		pnlButton.add(btnCalculate);
		btnCalculate.addActionListener(this);
		
		add(lblScore[0], BorderLayout.NORTH);
		add(btnCalculate, BorderLayout.SOUTH);
		add(pnlScore, BorderLayout.CENTER);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500,500);
		setLocationRelativeTo(null);
		setVisible(true);
		
	
	}

	public static void main(String[] args) {
		new Gui().setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
		// getting scores
		double s1 = Double.parseDouble(txtScore[0].getText());
		double s2 = Double.parseDouble(txtScore[1].getText());
		double s3 = Double.parseDouble(txtScore[2].getText());
		double s4 = Double.parseDouble(txtScore[3].getText());
		// getting weight
		double w1 = Double.parseDouble(txtWeight[0].getText());
		double w2 = Double.parseDouble(txtWeight[1].getText());
		double w3 = Double.parseDouble(txtWeight[2].getText());
		double w4 = Double.parseDouble(txtWeight[3].getText());
		// calculations
		double f1 = s1 * w1;
		double f2 = s2 * w2;
		double f3 = s3 * w3;
		double f4 = s4 * w4;
		// Avg weight
		double aw = f1 + f2 + f3 + f4;
		lblScore[0].setText("Your score is : " + aw );
	}
}
